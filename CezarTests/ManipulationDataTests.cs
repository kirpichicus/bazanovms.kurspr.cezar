﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Cezar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System.IO;

namespace Cezar.Tests
{
    [TestClass()]
    public class ManipulationDataTests
    {
        //минитест docx-парсера
        [TestMethod()]
        public void wordParseTest()
        {
            try
            {
                ManipulationData.wordParse("gfdgdfg");
                ManipulationData.wordParse("14");
                ManipulationData.wordParse("");
                ManipulationData.wordParse("test\\docx.docx");
                ManipulationData.wordParse("test\\txt.txt");
                ManipulationData.wordParse("test\\txt.docx");
                ManipulationData.wordParse("test\\docx.txt");
            }
            catch (Exception x)
            {
                Assert.Fail(x.Message);
            }
        }
        //минитест docx-сэйвера
        [TestMethod()]
        public void wordSaveTest()
        {
            try
            {
                ManipulationData.wordSave("gfdgdfg");
                ManipulationData.wordSave("14");
                ManipulationData.wordSave("");
                ManipulationData.wordSave("test\\docx.docx");
                ManipulationData.wordSave("test\\docx.docx");
                ManipulationData.wordSave("test\\txt.txt");
            }
            catch (Exception x)
            {
                Assert.Fail(x.Message);
            }
        }
        //минитест txt-парсера
        [TestMethod()]
        public void txtParseTest()
        {
            try
            {
                ManipulationData.txtParse("");
                ManipulationData.txtParse("gdf");
                ManipulationData.txtParse("12");
                ManipulationData.txtParse("test\\txt.docx");
                ManipulationData.txtParse("test\\txt.txt");
                ManipulationData.txtParse("test\\docx.txt");
            }
            catch (Exception x)
            {
                Assert.Fail(x.Message);
            }
        }
        //минитест txt-сэйвера
        [TestMethod()]
        public void txtSaveTest()
        {
            try
            {
                ManipulationData.txtSave("");
                ManipulationData.txtSave("gdf");
                ManipulationData.txtSave("12");
                ManipulationData.txtSave("test\\txt.docx");
                ManipulationData.txtSave("test\\txt.txt");
                ManipulationData.txtSave("test\\docx.txt");
            }
            catch (Exception x)
            {
                Assert.Fail(x.Message);
            }
        }
        //минитест шифровальщика информации, полученной из файлов
        [TestMethod()]
        public void CodeTextTest()
        {
            try
            {
                ManipulationData.CodeTextFile();
            }
            catch (Exception x)
            {
                Assert.Fail(x.Message);
            }
        }
        //минитест шифровальщика информации, введенный через клавиатуру
        [TestMethod()]
        public void CodeTextInputTest()
        {
            try
            {
                ManipulationData.CodeTextInput();
            }
            catch (Exception x)
            {
                Assert.Fail(x.Message);
            }
        }
        //минитест функции шифрования
        [TestMethod()]
        public void ToCesarTest()
        {
            try
            {
                ManipulationData.ToCesar("тестовая строка");
                ManipulationData.ToCesar("");
                ManipulationData.ToCesar("e");
            }
            catch (NullReferenceException)
            {
                return;
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }
        //минитест функции очистки буффера
        [TestMethod()]
        public void allClearTest()
        {
            try
            {
                ManipulationData.allClear();
            }
            catch (Exception x)
            {
                Assert.Fail(x.Message);
            }
        }

    }
}