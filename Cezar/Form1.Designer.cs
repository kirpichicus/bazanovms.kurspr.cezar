﻿namespace Cezar
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            Open = new System.Windows.Forms.Button();
            Save = new System.Windows.Forms.Button();
            saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            button1 = new System.Windows.Forms.Button();
            shiftSize = new System.Windows.Forms.NumericUpDown();
            aboutShiftSize = new System.Windows.Forms.Label();
            codedChooseFile = new System.Windows.Forms.RichTextBox();
            inputText = new System.Windows.Forms.RichTextBox();
            aboutChooseFile = new System.Windows.Forms.Label();
            aboutInputText = new System.Windows.Forms.Label();
            codedInputText = new System.Windows.Forms.RichTextBox();
            codeInputText = new System.Windows.Forms.Button();
            saveCodedInputText = new System.Windows.Forms.Button();
            saveFileDialog2 = new System.Windows.Forms.SaveFileDialog();
            isIncrementable = new System.Windows.Forms.CheckBox();
            fullFileName = new System.Windows.Forms.ListBox();
            CopyFile = new System.Windows.Forms.Button();
            button2 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(shiftSize)).BeginInit();
            SuspendLayout();
            // 
            // openFileDialog1
            // 
            openFileDialog1.FileName = "openFileDialog1";
            // 
            // Open
            // 
            Open.AccessibleDescription = "chooseFile";
            Open.Location = new System.Drawing.Point(192, 234);
            Open.Name = "Open";
            Open.Size = new System.Drawing.Size(75, 23);
            Open.TabIndex = 0;
            Open.Text = "Открыть";
            Open.UseVisualStyleBackColor = true;
            Open.Click += new System.EventHandler(button1_Click);
            // 
            // Save
            // 
            Save.AccessibleDescription = "saveChooseFile";
            Save.Location = new System.Drawing.Point(611, 234);
            Save.Name = "Save";
            Save.Size = new System.Drawing.Size(75, 23);
            Save.TabIndex = 2;
            Save.Text = "Сохранить";
            Save.UseVisualStyleBackColor = true;
            Save.Click += new System.EventHandler(button2_Click);
            // 
            // button1
            // 
            button1.AccessibleDescription = "codeChooseFile";
            button1.Location = new System.Drawing.Point(517, 234);
            button1.Name = "button1";
            button1.Size = new System.Drawing.Size(88, 23);
            button1.TabIndex = 3;
            button1.Text = "Зашифровать";
            button1.UseVisualStyleBackColor = true;
            button1.Click += new System.EventHandler(button1_Click_1);
            // 
            // shiftSize
            // 
            shiftSize.Location = new System.Drawing.Point(147, 7);
            shiftSize.Maximum = new decimal(new int[] {
            33,
            0,
            0,
            0});
            shiftSize.Minimum = new decimal(new int[] {
            33,
            0,
            0,
            -2147483648});
            shiftSize.Name = "shiftSize";
            shiftSize.Size = new System.Drawing.Size(120, 20);
            shiftSize.TabIndex = 6;
            shiftSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // aboutShiftSize
            // 
            aboutShiftSize.AutoSize = true;
            aboutShiftSize.Location = new System.Drawing.Point(9, 9);
            aboutShiftSize.Name = "aboutShiftSize";
            aboutShiftSize.Size = new System.Drawing.Size(136, 13);
            aboutShiftSize.TabIndex = 7;
            aboutShiftSize.Text = "Выберите размер сдвига";
            // 
            // codedChooseFile
            // 
            codedChooseFile.Location = new System.Drawing.Point(12, 263);
            codedChooseFile.Name = "codedChooseFile";
            codedChooseFile.Size = new System.Drawing.Size(855, 248);
            codedChooseFile.TabIndex = 8;
            codedChooseFile.Text = "";
            // 
            // inputText
            // 
            inputText.Location = new System.Drawing.Point(12, 52);
            inputText.Name = "inputText";
            inputText.Size = new System.Drawing.Size(855, 69);
            inputText.TabIndex = 9;
            inputText.Text = "";
            // 
            // aboutChooseFile
            // 
            aboutChooseFile.AutoSize = true;
            aboutChooseFile.Location = new System.Drawing.Point(12, 239);
            aboutChooseFile.Name = "aboutChooseFile";
            aboutChooseFile.Size = new System.Drawing.Size(177, 13);
            aboutChooseFile.TabIndex = 10;
            aboutChooseFile.Text = "Выберите файл для зашифровки:";
            // 
            // aboutInputText
            // 
            aboutInputText.AutoSize = true;
            aboutInputText.Location = new System.Drawing.Point(9, 36);
            aboutInputText.Name = "aboutInputText";
            aboutInputText.Size = new System.Drawing.Size(83, 13);
            aboutInputText.TabIndex = 11;
            aboutInputText.Text = "Введите текст:";
            // 
            // codedInputText
            // 
            codedInputText.Location = new System.Drawing.Point(12, 158);
            codedInputText.Name = "codedInputText";
            codedInputText.Size = new System.Drawing.Size(855, 73);
            codedInputText.TabIndex = 12;
            codedInputText.Text = "";
            // 
            // codeInputText
            // 
            codeInputText.Location = new System.Drawing.Point(12, 127);
            codeInputText.Name = "codeInputText";
            codeInputText.Size = new System.Drawing.Size(88, 23);
            codeInputText.TabIndex = 13;
            codeInputText.Text = "Зашифровать";
            codeInputText.UseVisualStyleBackColor = true;
            codeInputText.Click += new System.EventHandler(button2_Click_1);
            // 
            // saveCodedInputText
            // 
            saveCodedInputText.Location = new System.Drawing.Point(106, 127);
            saveCodedInputText.Name = "saveCodedInputText";
            saveCodedInputText.Size = new System.Drawing.Size(75, 23);
            saveCodedInputText.TabIndex = 14;
            saveCodedInputText.Text = "Сохранить";
            saveCodedInputText.UseVisualStyleBackColor = true;
            saveCodedInputText.Click += new System.EventHandler(button3_Click);
            // 
            // isIncrementable
            // 
            isIncrementable.AutoSize = true;
            isIncrementable.Location = new System.Drawing.Point(287, 8);
            isIncrementable.Name = "isIncrementable";
            isIncrementable.Size = new System.Drawing.Size(178, 17);
            isIncrementable.TabIndex = 15;
            isIncrementable.Text = "Автоинкрементировать сдвиг";
            isIncrementable.UseVisualStyleBackColor = true;
            // 
            // fullFileName
            // 
            fullFileName.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            fullFileName.FormattingEnabled = true;
            fullFileName.ItemHeight = 16;
            fullFileName.Location = new System.Drawing.Point(273, 237);
            fullFileName.Name = "fullFileName";
            fullFileName.Size = new System.Drawing.Size(238, 20);
            fullFileName.TabIndex = 16;
            fullFileName.SelectedIndexChanged += new System.EventHandler(fullFileName_SelectedIndexChanged);
            // 
            // CopyFile
            // 
            CopyFile.Location = new System.Drawing.Point(692, 234);
            CopyFile.Name = "CopyFile";
            CopyFile.Size = new System.Drawing.Size(75, 23);
            CopyFile.TabIndex = 17;
            CopyFile.Text = "Копировать";
            CopyFile.UseVisualStyleBackColor = true;
            CopyFile.Click += new System.EventHandler(button2_Click_2);
            // 
            // button2
            // 
            button2.Location = new System.Drawing.Point(187, 127);
            button2.Name = "button2";
            button2.Size = new System.Drawing.Size(75, 23);
            button2.TabIndex = 18;
            button2.Text = "Копировать";
            button2.UseVisualStyleBackColor = true;
            button2.Click += new System.EventHandler(button2_Click_3);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(882, 523);
            this.Controls.Add(button2);
            this.Controls.Add(CopyFile);
            this.Controls.Add(fullFileName);
            this.Controls.Add(isIncrementable);
            this.Controls.Add(saveCodedInputText);
            this.Controls.Add(codeInputText);
            this.Controls.Add(codedInputText);
            this.Controls.Add(aboutInputText);
            this.Controls.Add(aboutChooseFile);
            this.Controls.Add(inputText);
            this.Controls.Add(codedChooseFile);
            this.Controls.Add(aboutShiftSize);
            this.Controls.Add(shiftSize);
            this.Controls.Add(button1);
            this.Controls.Add(Save);
            this.Controls.Add(Open);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "Form1";
            this.Text = "Шифр Цезаря";
            ((System.ComponentModel.ISupportInitialize)(shiftSize)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private static System.Windows.Forms.Button button2;
        private static System.Windows.Forms.Button CopyFile;
        private static System.Windows.Forms.ListBox fullFileName;
        public static System.Windows.Forms.CheckBox isIncrementable;
        private static System.Windows.Forms.OpenFileDialog openFileDialog1;
        private static System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private static System.Windows.Forms.Label aboutShiftSize;
        private static System.Windows.Forms.SaveFileDialog saveFileDialog2;
        public static System.Windows.Forms.Button Open;
        public static System.Windows.Forms.Button Save;
        public static System.Windows.Forms.Button button1;
        public static System.Windows.Forms.RichTextBox codedChooseFile;
        public static System.Windows.Forms.RichTextBox inputText;
        public static System.Windows.Forms.Label aboutChooseFile;
        public static System.Windows.Forms.Label aboutInputText;
        public static System.Windows.Forms.RichTextBox codedInputText;
        public static System.Windows.Forms.Button codeInputText;
        public static System.Windows.Forms.Button saveCodedInputText;
        public static System.Windows.Forms.NumericUpDown shiftSize;
    }
}

