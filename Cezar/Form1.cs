﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Cezar
{
    public partial class Form1 : Form
    {
        public static List<string> cesarstringInput = new List<string>();// здесь хранится введенная шифровка
        public static List<string> filestringsFile = new List<string>();//здесь хранится текст из файла
        public static List<string> cesarstringFile = new List<string>();//здесь хранится шифровка из файла
        public static string filename = "";
        public static bool isFile = false;//проверка на "откуда брали текст"
        public static BindingList<string> filepathFull = new BindingList<string>();// для вывода имени открытого файла
        public static Regex regex = new Regex(@"[^/*<>|+%!@]+.docx$");//для проверки на docx
        public static Regex regex2 = new Regex(@"[^/*<>|+%!@]+.txt$");// для проверки на txt
        public Form1()
        {
            InitializeComponent();

            fullFileName.DataSource  = filepathFull ;
        }

        //открываем файл, который будем зашифровывать/расшифровывать
        public static void button1_Click(object sender, EventArgs e)
        {
            shiftSize.Value = 0;// чтобы сначала всегда был нулевой сдвиг
            try
            {
                if (openFileDialog1.ShowDialog() == DialogResult.Cancel)
                    return;
                filepathFull.Clear();
                filename = openFileDialog1.FileName;// получаем выбранный файл
                if (regex.IsMatch(filename))// проверка на docx и парсинг
                {
                    filepathFull.Add(filename);
                    ManipulationData.wordParse(filename);
                }
                else
                {
                    if (regex2.IsMatch(filename))// проверка на txt и парсинг
                    {
                        filepathFull.Add(filename);
                        ManipulationData.txtParse(filename);
                    }
                    else MessageBox.Show("Неверный формат файла");
                }
            }
            catch (Exception x)
            {
                MessageBox.Show("Ошибка открытия файла");
            }
        }
        // сохраняем тексты
        public static void button2_Click(object sender, EventArgs e)
        {
            isFile = true;
            try
            {
                if (saveFileDialog1.ShowDialog() == DialogResult.Cancel)
                    return;
                string filename = saveFileDialog1.FileName;// получаем выбранный файл
                if (regex.IsMatch(filename)) //проверка на docx, затем сэйв
                {
                    ManipulationData.wordSave(filename);
                }
                else
                {
                    if (regex2.IsMatch(filename))//проверка на txt, затем сэйв
                    {
                        ManipulationData.txtSave(filename);
                    }
                    else MessageBox.Show("Неверный формат файла");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения файла");
                return;
            }
        }
        // сохраняем текст, введенный с клавиатуры
        public static void button3_Click(object sender, EventArgs e)
        {
            isFile = false;
            try
            {

                if (saveFileDialog2.ShowDialog() == DialogResult.Cancel)
                    return;
                string filename = saveFileDialog2.FileName;// получаем выбранный файл
                if (regex.IsMatch(filename))//проверка на docx, затем сэйв
                {
                    ManipulationData.wordSave(filename);
                }
                else
                {
                    if (regex2.IsMatch(filename))//проверка на txt, затем сэйв
                    {
                        ManipulationData.txtSave(filename);
                    }
                    else MessageBox.Show("Неверный формат файла");
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения файла");
                return;
            }
        }
        //зашифровываем введенный текст
        public static void button2_Click_1(object sender, EventArgs e)
        {
            try
            {
                ManipulationData.CodeTextInput();
            }
            catch(Exception x)
            {
                MessageBox.Show("Ошибка шифрования");
            }
            
            
        }
        //зашифровываем считанный из файлов текст
        public static void button1_Click_1(object sender, EventArgs e)
        {
            try
            {
                ManipulationData.CodeTextFile();
            }
            catch (Exception x)
            {
                MessageBox.Show("Ошибка шифрования");
            }
        }
        //обновление информации в поле с именем выбранного файла
        private void fullFileName_SelectedIndexChanged(object sender, EventArgs e)
        {
            fullFileName.DataSource = filepathFull;
        }
        //копирование в буффер обмена из поля c зашифрованным/дешифрованным файлом
        private void button2_Click_2(object sender, EventArgs e)
        {
            string tmp="";
            foreach (var item in cesarstringFile)
            {
                tmp += item ;
            }
            Clipboard.SetText(tmp);
        }
        //копирование в буффер обмена из поля с зашифрованным/дешифрованным ввыденным текстом
        private void button2_Click_3(object sender, EventArgs e)
        {
            string tmp = "";
            foreach (var item in cesarstringInput)
            {
                tmp += item ;
            }
            Clipboard.SetText(tmp);
        }
    }
}
