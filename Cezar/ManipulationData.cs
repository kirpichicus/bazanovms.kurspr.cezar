﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Cezar
{
    public class ManipulationData
    {
        //парсим текст из docx
        public static void wordParse(string filename)
        {
            try
            {
                Form1.codedChooseFile.Text = "";
                Form1.filestringsFile.Clear();
                using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Open(filename, true))
                {
                    Body body = wordprocessingDocument.MainDocumentPart.Document.Body;//получаем доступ к телу файла

                    Form1.codedChooseFile.Text = "";
                    foreach (var item in body)// читаем построчно(абзацно) часть тела "с текстом" и выводим/запоминаем его
                    {
                        if (item.GetType().ToString() == "DocumentFormat.OpenXml.Wordprocessing.SectionProperties")
                        {
                            break;
                        }
                        Paragraph para = (Paragraph)item;
                        Form1.filestringsFile.Add(para.InnerText);
                        Form1.cesarstringFile.Add(para.InnerText);
                        Form1.codedChooseFile.Text += para.InnerText+"\n";
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка чтения файла");
            }
        }
        //сохраняем текст в docx
        public static void wordSave(string filename)
        {
            try
            {
                    List<string> tmp;//для хранение нужного списка строк
                    if (Form1.isFile)//определяем источник текста
                    {
                        tmp = Form1.cesarstringFile;
                    }
                    else
                    {
                        tmp = Form1.cesarstringInput;
                    }

                    if (string.IsNullOrEmpty(filename)) throw new Exception();
                    if (!Form1.regex.IsMatch(filename)) throw new Exception();
                    using (WordprocessingDocument wordprocessingDocument = WordprocessingDocument.Create(filename, DocumentFormat.OpenXml.WordprocessingDocumentType.Document))
                    {
                        MainDocumentPart mainPart = wordprocessingDocument.AddMainDocumentPart();
                        mainPart.Document = new Document();
                        // Вы реально читаете код??
                        Body body = mainPart.Document.AppendChild(new Body());
                        foreach (var item in tmp)//внутри body записываем строки(параграфы)
                        {
                            Paragraph para = body.AppendChild(new Paragraph());
                            Run run = para.AppendChild(new Run());
                            run.AppendChild(new Text(item));
                        }
                        wordprocessingDocument.Close();
                    }

                MessageBox.Show("Файл сохранен");

            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка сохранения файла");
                return;
            }
        }
        //парсим текст из txt
        public static void txtParse(string filename)
        {
            try
            {
                System.IO.StreamReader file = null;
                Form1.filestringsFile.Clear();
                Form1.codedChooseFile.Text = "";
                file = new System.IO.StreamReader(filename);//открываем файл
                string fileText;
                while ((fileText = file.ReadLine()) != null)//построчно выводим и запоминаем
                {
                    Form1.filestringsFile.Add(fileText);
                    Form1.cesarstringFile.Add(fileText);
                    Form1.codedChooseFile.Text +=fileText+ "\n";
                }
                file.Close();
            }
            catch (NullReferenceException x)
            {

            }
            catch (Exception x)
            {
                MessageBox.Show("Ошибка чтения файла");
            }
        }
        //сохраняем текст в txt
        public static void txtSave(string filename)
        {
            try
            {
                List<string> tmp;// для хранения нужного списка строк
                if (Form1.isFile)//определение источника текста
                {
                    tmp = Form1.cesarstringFile;
                }
                else
                {
                    tmp = Form1.cesarstringInput;
                }
                if (string.IsNullOrEmpty(filename)) throw new Exception();
                if (!Form1.regex2.IsMatch(filename)) throw new Exception();
                File.WriteAllLines(filename, tmp);
                MessageBox.Show("Файл сохранен");
            }
            catch(Exception x)
            {
                MessageBox.Show("Ошибка сохранения файла");
            }
        }
        //зашифровываем считанный из файлов текст
        public static void CodeTextFile()
        {
            try
            {
                Form1.cesarstringFile.Clear();
                if (Form1.isIncrementable.Checked)// если инкремент включен, инкрементируем
                if (!((int)Form1.shiftSize.Value >= Form1.shiftSize.Maximum))
                    Form1.shiftSize.Value++;
                Form1.codedChooseFile.Text = "";
                foreach (var item in Form1.filestringsFile) //построчно зашифровываем/дешифровываем и выводим/запоминаем
                {
                    string tmp = item;
                    tmp = ManipulationData.ToCesar(tmp);
                    Form1.codedChooseFile.Text += tmp + "\n";
                    Form1.cesarstringFile.Add(tmp);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ошибка шифрования");
            }

        }
        //зашифровываем введенный текст
        public static void CodeTextInput()
        {
            try
            {
                Form1.cesarstringInput.Clear();
                if (Form1.isIncrementable.Checked)
                    if (!((int)Form1.shiftSize.Value >= Form1.shiftSize.Maximum))// если инкремент включен, инкрементируем
                    Form1.shiftSize.Value++;
                string tmp = ToCesar(Form1.inputText.Text);
                if (tmp == "") { return; }
                Form1.cesarstringInput.Add(tmp[0].ToString());
                int k = 0;
                for (int i = 1; i < tmp.Length; i++) //построчно зашифровываем/дешифровываем и выводим/запоминаем
                {
                    if (tmp[i] == '\n')
                    {
                        Form1.cesarstringInput[k] += tmp[i];
                        Form1.cesarstringInput.Add("");
                        k++;
                        continue;
                    }
                    Form1.cesarstringInput[k] += tmp[i].ToString();
                }
                Form1.codedInputText.Text = tmp;
            }
            catch(Exception x)
            {
                MessageBox.Show("Ошибка шифрования");
            }
            
        }
        //переводим строку в шифр цезаря (сложный алгоритм)
        public static string ToCesar(string s)
        {
            char[] tmp = s.ToCharArray();
            string let = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
            List<char> letters = new List<char>(let);
            List<char> LETTERS = new List<char>(let.ToUpper());
            int shift = (int)Form1.shiftSize.Value;
            for (int i = 0; i < s.Length; i++)
            {
                int position;
                if (new List<char>(let).IndexOf(tmp[i]) != -1)
                {
                    position = letters.IndexOf(tmp[i]);
                    if ((position + shift < letters.Count) && (position + shift >= 0))
                    {
                        tmp[i] = letters[position + shift];
                    }
                    else
                    {
                        if (position + shift < 0)
                        {
                            tmp[i] = letters[letters.Count - (Math.Abs(position + shift))];
                        }
                        else
                        {
                            tmp[i] = letters[shift - (letters.Count - position)];
                        }
                    }
                }
                else
                {
                    if (new List<char>(let.ToUpper()).IndexOf(tmp[i]) != -1)
                    {
                        position = LETTERS.IndexOf(tmp[i]);
                        if ((position + shift < LETTERS.Count) && (position + shift >= 0))
                        {
                            tmp[i] = LETTERS[position + shift];
                        }
                        else
                        {
                            if (position + shift < 0)
                            {
                                tmp[i] = LETTERS[LETTERS.Count - (Math.Abs(position + shift))];
                            }
                            else
                            {
                                tmp[i] = LETTERS[shift - (LETTERS.Count - position)];
                            }
                        }
                    }
                    else continue;
                }
                
            }
            string ret = "";
            foreach (var item in tmp)
            {
                ret += item;
            }
            return ret;
        }
        // очищаем буферы
        public static void allClear()
        {
            try
            {
                Form1.cesarstringFile.Clear();
                Form1.filestringsFile.Clear();
                Form1.cesarstringInput.Clear();
            }
            catch(Exception)
            {

            }

        }
    }
}
